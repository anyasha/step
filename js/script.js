// ----our-services-navbar---
const ourServicesNav = document.querySelector('.section-navbar');
console.log(ourServicesNav);
const ourServicesContent = Array.from(document.querySelectorAll('.content'));
console.log(ourServicesContent);

ourServicesNav.addEventListener('click', event => {
    const clickedItem = event.target;
    const activeItem = ourServicesNav.querySelector('.active');
    if (clickedItem.classList.contains('our-services-navbar-item')) {
        clickedItem.classList.toggle('active');
        activeItem.classList.remove('active');
        // const data = clickedItem.dataset.tab;
        ourServicesContent.forEach((element) => {
          element.classList.remove('active');
          if (element.dataset.tab === clickedItem.dataset.tab) {
              element.classList.add('active')
          }
        })
    }
});

// ----our-amazing-work----
const amazingWorkNavbar = document.getElementById('amazing-work');
const amazingWorkGallery = document.querySelectorAll('.gallery-content');
const amazingWorkGalleryBlock = document.querySelector('.amazing-work-gallery');
const navItems = document.querySelectorAll('.amazing-work-navbar-item');
const loadBtn = document.getElementById('load-img');


showAll(amazingWorkGallery);

loadBtn.addEventListener('click', () => {
    loadBtn.remove();
    Array.from(document.querySelector('.hidden-gallery.hidden').children).forEach(element => {
        amazingWorkGalleryBlock.append(element)
    })
});

amazingWorkNavbar.addEventListener('click', event => {
    const clickedItem = event.target;
    Array.from(navItems).forEach(element => {
        element.classList.remove('active');
    });
    clickedItem.classList.add('active');
    filterImgs(amazingWorkGallery, clickedItem.dataset.section);
      if (clickedItem.dataset.section === 'all') {
      showAll(amazingWorkGallery);
    }
});

function filterImgs(imgCollection, dataName) {
    Array.from(imgCollection).forEach(element => {
        element.classList.remove('active');
        if (element.dataset.section === dataName) {
            element.classList.add('active')
        }
    })
}

function showAll(imgCollection) {
    Array.from(imgCollection).forEach(element => {
        element.classList.add('active')
    })
}

// ---Slider---

const sliderContainer = document.querySelector('.slider-container'); /*лента маленьких фото*/
const sliderBigContainer = document.querySelector('.slider-big-container'); /*лента блоков с большим фото*/
const review = document.querySelectorAll('.review');
const sliderLinks = document.querySelectorAll('.slider-link');
const viewPort = document.querySelector('.view-port');
const viewPortWidth = parseFloat(getComputedStyle(viewPort).width); /*задается в CSS*/
const bigViewPort = document.querySelector('.big-viewport');
const bigViewPortWidth = parseFloat(getComputedStyle(bigViewPort).width); /*задается в CSS*/
const controlPanel = document.querySelector('.slider-wrapper');
const sliderLeft = document.getElementById('slider-backward-btn');
const sliderRight = document.getElementById('slider-forward-btn');
const numberOfImg = Array.from(document.querySelectorAll('.slider-link')).length; /*кол-во фото в слайдере*/
const slideStep = 4; /*шаг слайдера*/
let clickedLinkIndex = 'slider-0';
// let firstVisibleLinkIndex = 'slider-0';


controlPanel.addEventListener('click', (event) => {
    const clickedItem = event.target;
    const activeItemMenu = sliderContainer.querySelector('.active');
    if (clickedItem.parentElement.classList.contains('slider-link')) {
        clickedItem.parentElement.classList.toggle('active');
        activeItemMenu.classList.remove('active');
        clickedLinkIndex = event.target.parentElement.dataset.pos;
        addClassListActive(review);
        let numberOfSlide = +clickedLinkIndex.split('-')[1];
        sliderBigContainer.style.left = `-${numberOfSlide*bigViewPortWidth}px`;
    } if (event.target === sliderLeft) {
        let numberOfSlide = +clickedLinkIndex.split('-')[1] - slideStep;
        if (numberOfSlide < 0) {
            numberOfSlide = 0;
        } clickedLinkIndex = clickedLinkIndex.split('-')[0] + '-' + numberOfSlide;
        sliderContainer.style.left = `-${numberOfSlide/slideStep*viewPortWidth}px`;
        sliderBigContainer.style.left = `-${numberOfSlide*bigViewPortWidth}px`;
        addClassListActive(review);
        addClassListActive(sliderLinks);
    } if (event.target === sliderRight) {
        let numberOfSlide = +clickedLinkIndex.split('-')[1] + slideStep;
        if (numberOfSlide > numberOfImg-slideStep) {
            numberOfSlide =  numberOfImg-slideStep;
        } clickedLinkIndex = clickedLinkIndex.split('-')[0] + '-' + numberOfSlide;
        sliderContainer.style.left = `-${numberOfSlide/slideStep*viewPortWidth}px`;
        sliderBigContainer.style.left = `-${numberOfSlide*bigViewPortWidth}px`;
        addClassListActive(review);
        addClassListActive(sliderLinks);

    }
});

function addClassListActive(selector) {
    Array.from(selector).forEach(element => {
        element.classList.remove('active');
        if (element.dataset.pos === clickedLinkIndex) {
            element.classList.add('active')
        }
    })
}





